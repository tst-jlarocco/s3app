S3Connection Test App
=====================

Building
========
```
cd geoplatform
git clone https://tst-jlarocco@bitbucket.org/tst-jlarocco/s3app.git
cd ./s3app/
./go
```

.s3config
=========
Create a JSON file named .s3config containing the connection information:

```
{
  "arn": "<some-arn>",
  "session-name": "test-session"
}
```

The 'arn' field should be the ARN for the account being accessed.
I needed to use the ARN for the gee-product-acct to see any buckets.

'session-name' can be an arbitrary string identifying the session.

To point to a different endpoint (like Moto), add an 'endpoint' entry:

```
{
  "arn": "<some-arn>",
  "session-name": "test-session",
  "entry": "http://localhost:5000"
}
```

Examples
========
```
./s3app help
./s3app buckets
./s3app list jl-test-bucket
./s3app put jl-test-bucket my-key-name "This is some data"
./s3app list jl-test-bucket my-key-name
./s3app get jl-test-bucket my-key-name
./s3app get opengee geep-fusion-tools-0.1.master.13-20171228.054623-1.tgz > gft.tgz
```
