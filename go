#!/usr/bin/env bash
# -*- sh -*-

rm -Rf build

if [ ! -f ./S3Connection.cpp ]; then
    ln -s ../geep-s3/plugin/src/S3Connection.cpp ./S3Connection.cpp
    ln -s ../geep-s3/plugin/src/S3Connection.h ./S3Connection.h
    ln -s ../geep-s3/plugin/src/AwsUtilities.h ./AwsUtilities.h
    ln -s ../geep-s3/plugin/src/AwsUtilities.cpp ./AwsUtilities.cpp
fi

mkdir build
pushd build

CMAKE=cmake
if [[ $(type -P cmake3) ]]; then
    CMAKE=cmake3
fi
$CMAKE .. -DCMAKE_BUILD_TYPE=Debug
make
popd

cp build/s3app .
