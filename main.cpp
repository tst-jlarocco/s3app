#include <aws/core/Aws.h>
#include <aws/core/auth/AWSCredentialsProvider.h>

#include <aws/s3/S3Client.h>
#include <aws/s3/model/ListBucketsResult.h>
#include <aws/s3/model/Bucket.h>
#include <aws/s3/model/ListObjectsV2Request.h>
#include <aws/s3/model/ListObjectsV2Result.h>
#include <aws/s3/model/HeadObjectRequest.h>
#include <aws/s3/model/HeadObjectResult.h>

#include <aws/sts/STSClient.h>

#include <aws/sts/model/AssumeRoleRequest.h>
#include <aws/sts/model/AssumeRoleResult.h>
#include <aws/sts/model/Credentials.h>

#include <aws/core/utils/memory/stl/SimpleStringStream.h>

#include <iomanip>
#include <iostream>
#include <stdexcept>
#include <vector>
#include <algorithm>
#include <fstream>
#include <iostream>

#include "S3Connection.h"
#include "AwsUtilities.h"


int main(int argc, char *argv[]) {

    GlobalAwsInit();

    std::shared_ptr<S3Connection> conn;

    std::ifstream fileIn;
    fileIn.open(".s3config");

    if (fileIn) {
        Aws::Utils::Json::JsonValue configFile(fileIn);
        Aws::Utils::Json::JsonView configuration = configFile.View();

        conn = std::make_shared<S3Connection>(configuration);
    } else {
        // If not using a .s3config, set a real ARN here
        conn = std::make_shared<S3Connection>();
    }


    std::vector<std::string> arguments(argv + 1, argv + argc);

    if ((arguments.size() == 0) || ((arguments.size() == 1) && (arguments[0] == "help"))) {
        std::cout << "S3 query tool/test application.\n\n";
        std::cout << "    s3app [help] [buckets] [list <bucket>] [put <bucket> <key> <data>] [get <bucket> <key>]\n\n\n";

    } else if ((arguments.size() == 1)
               && ("buckets" == arguments[0])) {
        std::vector<std::string> buckets;
        conn->ListBuckets(buckets);
        std::cout << "Buckets:\n";
        for (const auto & bucket : buckets) {
            std::cout << "* " << bucket << "\n";
        }
        std::cout << "\n";

    } else if (((arguments.size() == 2) || (arguments.size() == 3))
               && ("list" == arguments[0])) {
        std::string bucket = arguments[1];
        std::string prefix = "";
        if (arguments.size() == 3) {
            prefix = arguments[2];
        }
        std::vector<std::pair<std::string, size_t>> objects;
        conn->ListObjectsInBucket(bucket, objects, prefix);
        std::cout << "Objects in bucket \"" << bucket << "\":\n";
        for (const auto & md : objects) {
            std::cout << "  " <<  md.first  << " : " << md.second << "\n";
        }
        std::cout << "\n";


    }
    else if ((arguments.size() == 3)
             && ("get" == arguments[0])) {

        std::string bucket = arguments[1];
        std::string key = arguments[2];
        std::string data;
        conn->GetObject(bucket, key, data);
        std::cout << data;
    } else if ((arguments.size() == 4)
               && ("put" == arguments[0])) {

        std::string bucket = arguments[1];
        std::string key = arguments[2];
        std::string data = arguments[3];
        conn->PutObject(bucket, key, data);
    } else {
        std::cout << "Don't know what to do.\n";
    }

    return 0;
}
